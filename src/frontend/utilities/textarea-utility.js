/**
 * Provides helper methods for textareas and the like.
 *
 * @author derSennner
 */
class TextareaUtility {
    constructor() {
    }


    /**
     * Inserts the given text at the current caret position.
     *
     * @param {Element} textarea Textarea
     * @param {string} text Text to insert
     * @param {int} [position] Define a fixed caret position to insert at
     *
     * @returns {void}
     */
    static insertAtCaret(textarea, text, position) {
        let caretStartPosition = textarea.selectionStart;
        if (position !== undefined) {
            caretStartPosition = position;
        }

        let value = textarea.value;
        value = value.substring(0, caretStartPosition) + text + value.substring(caretStartPosition, value.length);
        textarea.value = value;

        textarea.focus();
        TextareaUtility.putCaretAtPosition(textarea, caretStartPosition + text.length);
    }


    /**
     * Wraps the current selection in the given start and end text.
     *
     * @param {Element} textarea Textarea
     * @param {string} startText Start of the wrap
     * @param {string} endText End of the wrap
     *
     * @returns {void}
     */
    static wrapSelection(textarea, startText, endText) {
        let caretStartPosition = textarea.selectionStart;
        let caretEndPosition = textarea.selectionEnd;

        let value = textarea.value;
        let selectedText = value.substring(caretStartPosition, caretEndPosition);

        value = value.substring(0, caretStartPosition) + startText + selectedText + endText + value.substring(caretEndPosition, value.length);
        textarea.value = value;

        textarea.focus();

        // If no selection was made, put caret in the middle
        if (caretStartPosition === caretEndPosition) {
            TextareaUtility.putCaretAtPosition(textarea, caretStartPosition + startText.length);
        }

        // Otherwise put caret after the wrap
        else {
            TextareaUtility.putCaretAtPosition(textarea, caretStartPosition + startText.length + selectedText.length + endText.length);
        }
    }


    /**
     * Puts the carete to the given position.
     *
     * @param {Element} textarea Textarea
     * @param {int} position Caret position
     *
     * @returns {void}
     */
    static putCaretAtPosition(textarea, position) {
        textarea.selectionStart = position;
        textarea.selectionEnd = position;
    }
}
