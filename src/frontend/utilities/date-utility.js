/**
 * Contains general functions for dealing with dates.
 *
 * @author FiveFuenf
 */
class DateUtility {
    constructor() {
    }


    /**
     * Gets the current UNIX timestamp.
     *
     * @returns {int}
     */
    static get timestamp() {
        return parseInt(Math.floor(Date.now() / 1000));
    }


    /**
     * Converts the timestamp into a date object.
     *
     * @param {int} timestamp UNIX timestamp
     *
     * @returns {string}
     */
    static convertTimestampToDate(timestamp) {
        return new Date(timestamp * 1000);
    }


    /**
     * Taktes a UNIX timestamp and converts into a descriptive string.
     *
     * @param {int} timestamp UNIX timestamp
     *
     * @returns {string}
     */
    static formatTimestamp(timestamp) {
        return DateUtility.formatDate(DateUtility.convertTimestampToDate(timestamp));
    }


    /**
     * Taktes a date object and converts into a string.
     *
     * @param {Date} date Date
     * @param {Object} [options] Date format options
     *
     * @returns {string}
     */
    static formatDate(date, options) {
        if (options === undefined) {
            options = {
                'weekday': 'long',
                'year': 'numeric',
                'month': 'long',
                'day': 'numeric',
                'hour': '2-digit',
                'minute': '2-digit'
            };
        }

        return date.toLocaleDateString('de-AT', options);
    }
}
