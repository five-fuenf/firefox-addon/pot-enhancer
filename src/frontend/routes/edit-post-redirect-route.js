/**
 * Route for the redirect after a post was edited.
 *
 * @author FiveFuenf
 * @deprecated Not really used anymore, as redirects to previous pages are made directly on the post/thread create pages via JavaScript
 */
class EditPostRedirectRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+editreply.php$/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < EditPostRedirectRoute.matchers.length; i++) {
            if (EditPostRedirectRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Immediately redirect
        DomUtility.body.classList.add('d-none');
        let redirectLink = document.querySelector('a.notice');
        window.location.href = redirectLink.getAttribute('href');
    }
}
