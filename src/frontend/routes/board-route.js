/**
 * Route for the board view (thread listing of a specific board).
 *
 * @author FiveFuenf
 */
class BoardRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];
        matchers.push(new RegExp(/^.+board.php\?BID=\d+$/));
        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < BoardRoute.matchers.length; i++) {
            if (BoardRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Adjust navbar
        let navbar = new Navbar();
    }
}
