/**
 * Route for creating a new post.
 *
 * @author FiveFuenf
 */
class CreatePostRoute extends Route {
    constructor() {
        super();
    }


    /**
     * @override
     */
    static get matchers() {
        let matchers = [];

        // Regular post creation
        matchers.push(new RegExp(/^.+newreply.php\?TID=\d+&SID=.*$/));

        // Quote another post
        matchers.push(new RegExp(/^.+newreply.php\?PID=\d+$/));

        return matchers;
    }


    /**
     * @override
     */
    isActive() {
        for (let i = 0; i < CreatePostRoute.matchers.length; i++) {
            if (CreatePostRoute.matchers[i].test(this._url)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Initializes this route.
     *
     * @returns {void}
     */
    initialize() {
        // Adjust navbar
        let navbar = new Navbar();

        // Block users
        if (!GeneralUtility.isEmpty(Settings.getInstance().get('blockUsers'))) {
            let blockUsers = new BlockUsers();
        }

        // Setup custom editor
        DomUtility.postFormContainer.classList.add('d-none');
        let editorContainer = document.createElement('div');
        GeneralUtility.insertBefore(editorContainer, DomUtility.postFormContainer);
        let editor = new PostEditor(editorContainer);
    }
}
