/**
 * Base class for controller.
 *
 * @author FiveFuenf
 */
class AbstractController {
    /**
     * Initializes the view.
     *
     * @param {View} view View associated to this controller
     */
    constructor(view) {
        // Contains the associated view of the controller
        this._view = view;
    }


    /**
     * Gets the view associated to the controller.
     *
     * @returns {View}
     */
    get view() {
        return this._view;
    }


    /**
     * Sets the view associated to the controller.
     *
     * @param {View} view View to associated to the controller
     * @returns {void}
     */
    set view(view) {
        this._view = view;
    }
}
