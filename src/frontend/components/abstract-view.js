/**
 * Base class for views.
 *
 * @author FiveFuenf
 */
class AbstractView {
    /**
     * Initializes the view.
     *
     * @param {Element} [container=null] Main container of the view
     */
    constructor(container) {
        if (container === undefined) {
            container = null;
        }

        // Holds the main container of the view
        this._container = container;

        // Contains any observers listening to view changes
        this._observers = {};
    }


    /**
     * Gets the main container of the view.
     *
     * @returns {Element}
     */
    get container() {
        return this._container;
    }


    /**
     * Sets the main container of the view.
     *
     * @param {Element} container New main container
     * @returns {void}
     */
    set container(container) {
        this._container = container;
    }


    /**
     * Gets all registerted observers.
     *
     * @returns {JSON}
     */
    get observers() {
        return this._observers;
    }


    /**
     * Sets possible observers.
     *
     * @param {JSON} observers Observers to set
     * @returns {void}
     */
    set observers(observers) {
        this._observers = observers;
    }


    /**
     * Adds a listener to the given event type.
     *
     * @param {string} type What event type to listen to
     * @param {function} callback Callback to the observer
     * @returns {void}
     */
    on(type, callback) {
        if (!this._observers.hasOwnProperty(type)) {
            throw new Error('Event of type "' + type + '" not recognized.');
        }
        this._observers[type].push(callback);
    }


    /**
     * Triggers the given event.
     *
     * @param {string} type What event type to trigger
     * @param {*} [callbackParameters=AbstractView] Parameters to pass to the callback
     * @returns {void}
     */
    trigger(type, callbackParameters) {
        if (!this._observers.hasOwnProperty(type)) {
            throw new Error('Event of type "' + type + '" not recognized.');
        }

        if (callbackParameters === undefined) {
            callbackParameters = this;
        }

        for (let i = 0; i < this._observers[type].length; i++) {
            if (this._observers[type][i] === undefined) {
                throw new Error('Observer listening to "' + type + '" is undefined.');
            }
            this._observers[type][i](callbackParameters);
        }
    }


    /**
     * Renders the view into the current main container.
     *
     * @returns {Element} Rendered output
     * @throws When the view has no container
     */
    render() {
    }


    /**
     * Initializes DOM events for the rendered view.
     *
     * @throws When the view has no container
     */
    initializeDom() {
    }
}
