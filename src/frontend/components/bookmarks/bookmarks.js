/**
 * View for adjusting the bookmark list.
 *
 * @author FiveFuenf
 */
class Bookmarks extends AbstractView {
    /**
     * @override
     */
    constructor(container) {
        super(container);

        this.initializeDom();
    }


    /**
     * @override
     */
    render() {
        super.render();
    }


    /**
     * @override
     */
    initializeDom() {
        super.initializeDom();

        // Better visibility for hot/deprecated bookmarks
        let bookmarks = DomUtility.bookmarksContainer;
        if (!bookmarks) {
            return;
        }

        // Active and hot bookmarks
        let hotBookmarks = bookmarks.querySelectorAll('img[src$="mini-folder-new.gif"]');
        if (hotBookmarks) {
            hotBookmarks.forEach(bookmark => {
                bookmark.parentElement.parentElement.classList.add('hot');
            });
        }

        // Closed bookmarks with still unread posts
        let closedBookmarksAndUnread = bookmarks.querySelectorAll('img[src$="mini-closed-folder-new.gif"]');
        if (closedBookmarksAndUnread) {
            closedBookmarksAndUnread.forEach(bookmark => {
                bookmark.parentElement.parentElement.classList.add('closed-unread');
            });
        }

        // Closed bookmarks with no unread posts
        let closedBookmarksAndRead = bookmarks.querySelectorAll('img[src$="mini-closed-folder.gif"]');
        if (closedBookmarksAndRead) {
            closedBookmarksAndRead.forEach(bookmark => {
                bookmark.parentElement.parentElement.classList.add('closed-read');
            });
        }

        // Put bookmarks next to the board overview
        let bookmarksNextToBoard = Settings.getInstance().get('functions.bookmarksNextToBoard');
        if (bookmarksNextToBoard === 'left') {
            DomUtility.body.classList.add('bookmarks-next-to-board', 'bookmarks-next-to-board-left');
        } else if (bookmarksNextToBoard === 'right') {
            DomUtility.body.classList.add('bookmarks-next-to-board', 'bookmarks-next-to-board-right');
        }

        // Add open-in-new-tab link
        if (Settings.getInstance().get('functions.showOpenInNewTabLinkForBookmarks')) {
            this.addOpenInNewTabLinks(bookmarks);
        }

        // Add pin-bookmark button
        if (Settings.getInstance().get('functions.showOpenInNewTabLinkForBookmarks')) {
            this.addPinFunction();
        }

        // Apply custom sorting
        let sorting = Settings.getInstance().get('functions.bookmarksSorting');
        if (sorting) {
            this.applySorting(sorting);
        }

        // Apply pinned bookmark sorting
        let pinnedBookmarks = Settings.getInstance().get('pinnedBookmarks');
        if (pinnedBookmarks.length > 0) {
            this.applyPinnedBookmarksSorting();
        }
    }


    /**
     * Adds an open-in-new-tab-link to the bookmark items
     *
     * @returns {void}
     */
    addOpenInNewTabLinks() {
        var items = DomUtility.bookmarkItems;
        if (!items || items.length === 0) {
            return;
        }

        items.forEach(bookmark => {
            var title = bookmark.querySelectorAll('td')[2];
            if (title) {
                var href = title.querySelector('a').getAttribute('href');
                var link = document.createElement('a');
                link.classList.add('open-in-new-tab');
                link.innerText = '⇲';
                link.setAttribute('href', href);
                link.setAttribute('target', '_blank');

                title.append(link);
            }
        });
    }


    /**
     * Allows to pin a bookmark to the top of the list by double-clicking it.
     *
     * @returns {void}
     */
    addPinFunction() {
        var items = DomUtility.bookmarkItems;
        if (!items || items.length === 0) {
            return;
        }

        // Get current set of pinned bookmarks to mark bookmarks with a is-pinned-flag
        let pinnedBookmarks = Settings.getInstance().get('pinnedBookmarks');
        DebugUtility.log('Current bookmarks:')
        DebugUtility.log(pinnedBookmarks);

        // Remember all thread IDs to later remove deprecated pinned bookmarks
        let tids = [];

        items.forEach(bookmark => {
            // Find TID from bookmarks
            let title = bookmark.querySelectorAll('td')[2];
            let tid = '';
            if (title) {
                let link = title.querySelector('a');
                tid = RegExUtility.extractTidFromUrl(link.getAttribute('href'));
                tids.push(tid);

                if (pinnedBookmarks.indexOf(tid) !== -1) {
                    bookmark.classList.add('is-pinned');
                }

                // The method applyPinnedBookmarksSorting is called after the applySorting method
            }

            bookmark.addEventListener('dblclick', () => {
                if (tid) {
                    let pinnedBookmarks = Settings.getInstance().get('pinnedBookmarks');
                    let index = pinnedBookmarks.indexOf(tid);

                    if (index === -1) {
                        DebugUtility.log('Pinning bookmark with TID ' + tid + '.');
                        pinnedBookmarks.push(tid);
                        Settings.getInstance().set('pinnedBookmarks', pinnedBookmarks);
                        bookmark.classList.add('is-pinned');
                    } else {
                        DebugUtility.log('Unpinning bookmark with TID ' + tid + '.');
                        pinnedBookmarks.splice(index, 1);
                        Settings.getInstance().set('pinnedBookmarks', pinnedBookmarks);
                        bookmark.classList.remove('is-pinned');
                    }

                    // Reapply sorting
                    this.applyPinnedBookmarksSorting();
                }
            });
        });

        DebugUtility.log('TIDs found in bookmark list:');
        DebugUtility.log(tids);

        // Remove any deprecated pinned bookmarks
        let cleanedPinnedBookmarks = [];
        DebugUtility.log('Removing deprecated pinned bookmarks.');

        for (let pinnedBookmark of pinnedBookmarks) {
            if (tids.indexOf(pinnedBookmark) !== -1) {
                cleanedPinnedBookmarks.push(pinnedBookmark)
            } else {
                DebugUtility.log('Removing bookmark with TID ' + pinnedBookmark + ', as it was no longer present in the bookmark list.');
            }
        }

        Settings.getInstance().set('pinnedBookmarks', cleanedPinnedBookmarks);
        DebugUtility.log('Cleaned bookmarks:')
        DebugUtility.log(cleanedPinnedBookmarks);
    }


    /**
     * Applies custom sorting to the bookmark items.
     *
     * @param {string} sorting What to sort by
     *
     * @returns {void}
     */
    applySorting(sorting) {
        var items = DomUtility.bookmarkItems;
        if (!items || items.length === 0) {
            return;
        }

        if (sorting === 'unread') {
            // Raw check if any unread post are available at all; if no, then stop sorting immediately
            if (!DomUtility.bookmarksContainer.querySelector('strong')) {
                return;
            }

            // Sorting only works with arrays, not with NodeLists
            var itemsArray = [].slice.call(items);
            itemsArray.sort(function (a, b) {
                // Extract unread-counts from the bookmark items
                var unreadCountA = 0;
                var unreadCountAElement = a.querySelector('strong');

                if (unreadCountAElement) {
                    unreadCountA = parseInt(unreadCountAElement.innerText);

                    if (isNaN(unreadCountA)) {
                        unreadCountA = 0;
                    }
                }

                var unreadCountB = 0;
                var unreadCountBElement = b.querySelector('strong');

                if (unreadCountBElement) {
                    unreadCountB = parseInt(unreadCountBElement.innerText);

                    if (isNaN(unreadCountB)) {
                        unreadCountB = 0;
                    }
                }

                if (unreadCountA > unreadCountB) {
                    return -1;
                } else if (unreadCountA < unreadCountB) {
                    return 1;
                } else {
                    return 0;
                }
            });
        }

        // Empty current table and replace with newly ordered items
        let table = DomUtility.bookmarksContainer.querySelector('table');
        let tbody = table.querySelector('tbody');

        table.removeChild(tbody);
        tbody = document.createElement('tbody');
        table.append(tbody);

        itemsArray.forEach(bookmark => {
            tbody.append(bookmark);
        });

        // Reset old list of bookmarks
        DomUtility._bookmarkItems = null;
    }


    /**
     * Moves pinned bookmarks to the top of the bookmark list.
     *
     * @returns {void}
     */
    applyPinnedBookmarksSorting() {
        var items = DomUtility.bookmarkItems;
        if (!items || items.length === 0) {
            return;
        }

        let table = DomUtility.bookmarksContainer.querySelector('table');
        let tbody = table.querySelector('tbody');

        items.forEach(bookmark => {
            if (bookmark.classList.contains('is-pinned')) {
                tbody.insertAdjacentElement('afterbegin', bookmark);
            }
        });
    }
}
