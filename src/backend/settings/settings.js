/**
 * Singleton containing all user settings.
 * Settings are saved synchronized in the cloud and locally. Initially or after a browser clear-cache wipe the settings are retrieved from sync.
 * Once they have been loaded once, they will be accessed through local storage. Updating the settings will update both the local and synced storages.
 *
 * @author FiveFuenf
 */
class Settings {
    constructor() {
        // Contains all user settings
        this._settings = this.defaultSettings;

        // Contains observes of this objects
        this._observers = {
            'change': []
        };

        // Register own event listener for persisting data in local storage
        this.on('change', () => {
            DebugUtility.log('Settings changed, save.');
            this.save();
        });
    }


    /**
     * Gets the singleton instance.
     *
     * @returns {Settings}
     */
    static getInstance() {
        if (Settings._instance === null) {
            Settings._instance = new Settings();
        }
        return Settings._instance;
    }


    /**
     * Registers an observer to the given event type.
     *
     * @param {string} type Event
     * @param {function} callback Event callback
     *
     * @returns {void}
     * @throws When the event was not found
     */
    on(type, callback) {
        if (!this._observers.hasOwnProperty(type)) {
            throw new Error('Event "' + type + '" not found.');
        }

        this._observers[type].push(callback);
    }


    /**
     * Triggers a specific event.
     *
     * @param {string} type Event
     *
     * @returns {void}
     * @throws When the event was not found
     */
    trigger(type) {
        if (!this._observers.hasOwnProperty(type)) {
            throw new Error('Event "' + type + '" not found.');
        }

        for (let i = 0; i < this._observers[type].length; i++) {
            this._observers[type][i](this);
        }
    }


    /**
     * Gets the default settings.
     *
     * @returns {JSON}
     */
    get defaultSettings() {
        return {
            // General addon functions
            'functions': {
                'rememberPostMessage': true,
                'rememberPostMessageText': '', // Contains the remembered post message txt, if rememberPostMessage is enabled
                'bookmarksNextToBoard': '',
                'showOpenInNewTabLinkForBookmarks': true,
                'bookmarkPins': true,
                'bookmarksSorting': '',
                'responsiveLayout': true,
                'debug': false
            },

            // Contains pinned bookmarks; list will be updated automatically if bookmarks are not present anymore; values are TIDs to the thread
            'pinnedBookmarks': [],

            // Saved/bookmarked posts; keys are PIDs to the post
            'savePosts': {},

            // CSS code to inject
            'injectCssCode': '',

            // Custom emotes
            // Identifiers are used as keys for ease of access
            'emotes': {
                // 'EmoteName': {
                //     'identifier': 'EmoteName',
                //     'url': 'https://url/to/image.png'
                // }
                'FerdAerger': {
                    'identifier': 'FerdAerger',
                    'url': 'https://i.imgur.com/EvHTsCR.png'
                },
                'FerdAugen': {
                    'identifier': 'FerdAugen',
                    'url': 'https://i.imgur.com/kYAtR2L.png'
                },
                'FerdBah': {
                    'identifier': 'FerdBah',
                    'url': 'https://i.imgur.com/bkFr0WV.png'
                },
                'FerdBlorg': {
                    'identifier': 'FerdBlorg',
                    'url': 'https://i.imgur.com/57LKD3H.png'
                },
                'FerdDenken': {
                    'identifier': 'FerdDenken',
                    'url': 'https://i.imgur.com/D6x3o52.png'
                },
                'FerdHuea': {
                    'identifier': 'FerdHuea',
                    'url': 'https://i.imgur.com/lY6droK.gif'
                },
                'FerdKaffee': {
                    'identifier': 'FerdKaffee',
                    'url': 'https://i.imgur.com/xeTpXWo.png'
                },
                'FerdSchoen': {
                    'identifier': 'FerdSchoen',
                    'url': 'https://i.imgur.com/XmfGXSf.png'
                },
                'FerdSchweiss': {
                    'identifier': 'FerdSchweiss',
                    'url': 'https://i.imgur.com/lACa4fI.png'
                },
                'FerdUhr': {
                    'identifier': 'FerdUhr',
                    'url': 'https://i.imgur.com/Yt2FA0x.png'
                },
                'FerdWas': {
                    'identifier': 'FerdWas',
                    'url': 'https://i.imgur.com/275ayFn.png'
                },
                'FerdWeimen': {
                    'identifier': 'FerdWeimen',
                    'url': 'https://i.imgur.com/RRuKQjU.png'
                },
                'FerdWzzz': {
                    'identifier': 'FerdWzzz',
                    'url': 'https://i.imgur.com/p7latIn.png'
                }
            },

            // User blocking
            'blockUsers': {
                // 'usernameToBlock': {
                //     'name': 'usernameToBlock'
                // }
            }
        };
    }


    /**
     * Gets all current settings.
     *
     * @returns {JSON}
     */
    get settings() {
        return this._settings;
    }


    /**
     * Sets and overwrites a fresh set of settings.
     *
     * @param {JSON} settings New settings
     *
     * @returns {void}
     */
    set settings(settings) {
        this._settings = settings;
        this.trigger('change');
    }


    /**
     * Loads settings from the storage. Should always be called initially by the app.
     *
     * @returns {Promise}
     */
    load() {
        DebugUtility.log('Load settings.');

        // Get settings from local cache, if possible
        return browser.storage.local.get('settings')
            .then(localResults => {
                DebugUtility.log('Local settings promise.');
                DebugUtility.log(GeneralUtility.clone(localResults));

                // There are no local settings saved yet, so get settings from sync
                if (GeneralUtility.isEmpty(localResults)) {
                    DebugUtility.log('Local settings empty.');
                    DebugUtility.log('Loading settings from sync.');

                    return browser.storage.sync.get('settings')
                        .then(syncResults => {
                            DebugUtility.log('Sync settings promise 1.');
                            DebugUtility.log(GeneralUtility.clone(syncResults));

                            this._settings = this.parse(syncResults.settings);
                        })
                        .then(() => {
                            DebugUtility.log('Sync settings promise 2.');
                            // Cache settings from sync
                            browser.storage.local.set({
                                'settings': this._settings
                            });
                        });
                }

                // There are locally cached settings, so don't bother connecting to sync
                DebugUtility.log('Local settings present.');
                DebugUtility.log('Loading settings locally.');

                this._settings = this.parse(localResults.settings);
            });
    }


    /**
     * Saves settings into the storage.
     *
     * @returns {Promise}
     */
    save() {
        // Update cache
        browser.storage.local.set({
            'settings': GeneralUtility.clone(this._settings)
        });

        // Update cache in cloud sync
        return browser.storage.sync.set({
            'settings': GeneralUtility.clone(this._settings)
        });
    }


    /**
     * Parses a serialized settings object back into usable settings.
     * Note that the settings are not really checked for validity. If for whatever reason the settings might be malformed, an error would be thrown. The user
     * should then simply reset the settings. Not checking for validity makes it easier to add new option fields and still preserve the current user settings.
     *
     * @param {Object} settings Raw, serialized settings
     *
     * @returns {Object} Parsed settings
     */
    parse(settings) {
        DebugUtility.log('Parse settings.');

        let defaultSettings = this.defaultSettings;

        // No settings are saved => use default settings
        if (GeneralUtility.isEmpty(settings)) {
            DebugUtility.log('Settings were empty, use default settings.');
            settings = defaultSettings;
        }
        // Validate saved settings
        else {
            // Add any missing values from the default settings
            settings = this.addDefaultValues(settings, defaultSettings);
        }

        // Deserialize objects
        for (let i = 0; i < settings.emotes.length; i++) {
            settings.emotes[i] = Emote.deserialize(settings.emotes[i]);
        }

        for (let i = 0; i < settings.blockUsers.length; i++) {
            settings.blockUsers[i] = BlockUser.deserialize(settings.blockUsers[i]);
        }

        DebugUtility.log('Settings parsed.');
        DebugUtility.log(GeneralUtility.clone(settings));

        return settings;
    }


    /**
     * Recursively adds any missing keys in the settings object by using values from the default settings object.
     * This method will not remove any additional values found in settings.
     *
     * @param {Object} settings Base settings
     * @param {Object} defaultSettings What base settings should look like
     *
     * @returns {Object} Settings with added default values
     */
    addDefaultValues(settings, defaultSettings) {
        let keys = Object.keys(settings);
        let defaultKeys = Object.keys(defaultSettings);

        defaultKeys.forEach(defaultKey => {
            // Ignore emotes, otherwise the default "Ferd"-emotes would be re-added all the time, even after the user deleted them
            if (defaultKey === 'emotes') {
                return;
            }

            // If a key is missing, add the default value
            if (keys.indexOf(defaultKey) === -1) {
                DebugUtility.log(`Setting with key "${defaultKey}" was missing. Using default value for this settings.`, true);
                settings[defaultKey] = defaultSettings[defaultKey];
            }

            // If the key exists and the setting is another sub-object also add default values to that sub-object
            else if (typeof settings[defaultKey] === 'object') {
                settings[defaultKey] = this.addDefaultValues(settings[defaultKey], defaultSettings[defaultKey]);
            }
        });

        return settings;
    }


    /**
     * Resets all settings to default.
     *
     * @returns {void}
     */
    reset() {
        this._settings = this.defaultSettings;
        this.trigger('change');
        window.location.reload();
    }


    /**
     * Gets an option.
     *
     * @param {string|Array} path Dot-separated path to get or array with strings representing the path
     *
     * @returns {*}
     */
    get(path) {
        return GeneralUtility.getByPath(path, this._settings);
    }


    /**
     * Sets an option.
     *
     * @param {string|Array} path Dot-separated path to the element to set or array with strings representing the path
     * @param {*} value  Value to save
     *
     * @returns {void}
     */
    set(path, value) {
        GeneralUtility.setByPath(path, value, this._settings);
        this.trigger('change');
    }


    /**
     * Remove an option.
     *
     * @param {string|Array} path Dot-separated path to get or array with strings representing the path
     *
     * @returns {void}
     */
    remove(path) {
        GeneralUtility.removeByPath(path, this._settings);
        this.trigger('change');
    }
}


// Contains the singleton instance
Settings._instance = null;
