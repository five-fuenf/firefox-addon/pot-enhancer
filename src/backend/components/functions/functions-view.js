/**
 * View for adjusting general functions.
 *
 * @author FiveFuenf
 */
class FunctionsView extends AbstractView {
    constructor(container) {
        super(container);

        // Contains the checkbox to turn on/off the compact layout
        this.rememberPostMessageCheckbox = this._container.querySelector('#functionsRememberPostMessage');

        // Checkboxes related to where the bookmarks are placed
        this.bookmarksNextToBoardCheckbox = this._container.querySelector('#functionsBookmarksNextToBoardDisable');
        this.bookmarksNextToBoardLeftCheckbox = this._container.querySelector('#functionsBookmarksNextToBoardLeft');
        this.bookmarksNextToBoardRightCheckbox = this._container.querySelector('#functionsBookmarksNextToBoardRight');

        // Checkbox for showing open-in-new-tab links for bookmarks
        this.showOpenInNewTabLinkForBookmarksCheckbox = this._container.querySelector('#functionsShowOpenInNewTabLinkForBookmarks');

        // Checkbox for enabling bookmark pins
        this.bookmarkPins = this._container.querySelector('#functionsBookmarkPins');

        // Selection for bookmark order
        this.bookmarksSortingSelect = this._container.querySelector('#functionsBookmarksSorting');

        // Checkbox for enabling the responsive layout
        this.responsiveLayoutCheckbox = this._container.querySelector('#functionsResponsiveLayout');

        // Debug enable switch
        this.debugCheckbox = this._container.querySelector('#functionsDebug');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        // Handle rememeber post message setting
        if (Settings.getInstance().get('functions.rememberPostMessage')) {
            this.rememberPostMessageCheckbox.checked = true;
        }

        this.rememberPostMessageCheckbox.addEventListener('change', () => {
            if (this.rememberPostMessageCheckbox.checked) {
                Settings.getInstance().set('functions.rememberPostMessage', true);
            } else {
                Settings.getInstance().set('functions.rememberPostMessage', false);
            }
        });


        // Handle bookmarks next to board overview setting
        let bookmarksNextToBoard = Settings.getInstance().get('functions.bookmarksNextToBoard');
        if (bookmarksNextToBoard === 'left') {
            this.bookmarksNextToBoardLeftCheckbox.checked = true;
        } else if (bookmarksNextToBoard === 'right') {
            this.bookmarksNextToBoardRightCheckbox.checked = true;
        } else {
            this.bookmarksNextToBoardCheckbox.checked = true;
        }

        let setBookmarksNextToBoard = () => {
            if (this.bookmarksNextToBoardLeftCheckbox.checked) {
                Settings.getInstance().set('functions.bookmarksNextToBoard', 'left');
            } else if (this.bookmarksNextToBoardRightCheckbox.checked) {
                Settings.getInstance().set('functions.bookmarksNextToBoard', 'right');
            } else if (this.bookmarksNextToBoardCheckbox.checked) {
                Settings.getInstance().set('functions.bookmarksNextToBoard', '');
            }
        };

        this.bookmarksNextToBoardLeftCheckbox.addEventListener('change', setBookmarksNextToBoard);
        this.bookmarksNextToBoardRightCheckbox.addEventListener('change', setBookmarksNextToBoard);
        this.bookmarksNextToBoardCheckbox.addEventListener('change', setBookmarksNextToBoard);


        // Handle bookmarks-in-new-tab-link checkbox
        if (Settings.getInstance().get('functions.showOpenInNewTabLinkForBookmarks')) {
            this.showOpenInNewTabLinkForBookmarksCheckbox.checked = true;
        }

        this.showOpenInNewTabLinkForBookmarksCheckbox.addEventListener('change', () => {
            if (this.showOpenInNewTabLinkForBookmarksCheckbox.checked) {
                Settings.getInstance().set('functions.showOpenInNewTabLinkForBookmarks', true);
            } else {
                Settings.getInstance().set('functions.showOpenInNewTabLinkForBookmarks', false);
            }
        });


        // Handle bookmarks-pins checkbox
        if (Settings.getInstance().get('functions.bookmarkPins')) {
            this.bookmarkPins.checked = true;
        }

        this.bookmarkPins.addEventListener('change', () => {
            if (this.bookmarkPins.checked) {
                Settings.getInstance().set('functions.bookmarkPins', true);
            } else {
                Settings.getInstance().set('functions.bookmarkPins', false);
            }
        });


        // Handle bookmarks-sorting select
        var bookmarksSortingValue = Settings.getInstance().get('functions.bookmarksSorting');
        if (bookmarksSortingValue) {
            var bookmarksSortingOption = this.bookmarksSortingSelect.querySelector('[value="' + bookmarksSortingValue + '"]');
            if (bookmarksSortingOption) {
                this.bookmarksSortingSelect.value = bookmarksSortingValue;
            }
        }

        this.bookmarksSortingSelect.addEventListener('change', () => {
            var sorting = this.bookmarksSortingSelect.value;
            if (sorting === undefined || !sorting) {
                sorting = '';
            }

            Settings.getInstance().set('functions.bookmarksSorting', sorting);
        });


        // Handle responsive-layout checkbox
        if (Settings.getInstance().get('functions.responsiveLayout')) {
            this.responsiveLayoutCheckbox.checked = true;
        }

        this.responsiveLayoutCheckbox.addEventListener('change', () => {
            if (this.responsiveLayoutCheckbox.checked) {
                Settings.getInstance().set('functions.responsiveLayout', true);
            } else {
                Settings.getInstance().set('functions.responsiveLayout', false);
            }
        });


        // Handle debug enable switch
        if (Settings.getInstance().get('functions.debug')) {
            this.debugCheckbox.checked = true;
        }

        this.debugCheckbox.addEventListener('change', () => {
            if (this.debugCheckbox.checked) {
                Settings.getInstance().set('functions.debug', true);
            } else {
                Settings.getInstance().set('functions.debug', false);
            }
        });
    }
}
