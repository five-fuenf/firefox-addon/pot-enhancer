/**
 * Controls emote creation and removing.
 *
 * @author FiveFuenf
 */
class EmotesController extends AbstractController {
    constructor(view) {
        super(view);

        this.view.on('save', (e) => this.save(e));
        this.view.on('remove', (e) => this.remove(e));
    }


    /**
     * Saves a new emote.
     *
     * @param {*} event Event parameters
     *
     * @returns {void}
     * @throws When the identifier already exists
     */
    save(event) {
        let emote = new Emote(event.identifier, event.url);
        let emotes = Settings.getInstance().get('emotes');

        // If this is a call for a fresh, not yet saved emote, check if identifier
        // already exists
        if (event.isNew !== undefined && event.isNew && emotes.hasOwnProperty(emote.identifier)) {
            throw new Error('Emote with identifier "' + emote.identifier + '" already exists.');
        }

        // If the emote was already saved, but the identifier is changed, remove
        // old emote
        if (event.identifier !== event.previousIdentifier) {
            this.remove(event.previousIdentifier);
        }

        emotes[emote.identifier] = emote.serialize();
        Settings.getInstance().set('emotes', emotes);
    }


    /**
     * Removes an emote.
     *
     * @param {string} identifier Identifier of the emote to remove
     *
     * @returns {void}
     */
    remove(identifier) {
        let path = ['emotes', identifier];
        Settings.getInstance().remove(path);
    }
}
