/**
 * View for importing user settings.
 *
 * @author FiveFuenf
 */
class ImportSettingsView extends AbstractView {
    constructor(container) {
        super(container);

        // Contains input field for selecting and importing a settings file
        this.importInput = this._container.querySelector('input');

        // Button starting the import
        this.importButton = this._container.querySelector('button');

        // Container for status messages
        this.statusContainer = document.querySelector('#settingsStatus');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        this.importButton.addEventListener('click', () => {
            // Sanity checks
            if (this.importInput.files.length !== 1) {
                this.statusContainer.append(GeneralUtility.createAlert('warning', 'No file selected.', 5000));
                return;
            }

            let file = this.importInput.files[0];
            if (!GeneralUtility.isFileExtension(file.name, 'json')) {
                this.statusContainer.append(GeneralUtility.createAlert('warning', 'Invalid file, should be "*.json".', 5000));
                return;
            }

            // Read file and set as settings
            let reader = new FileReader();
            reader.addEventListener('load', () => {
                try {
                    let loadedSettings = JSON.parse(reader.result);
                    Settings.getInstance().settings = loadedSettings;
                    this.statusContainer.append(GeneralUtility.createAlert('success', 'File successfully imported.', 5000));
                } catch (e) {
                    this.statusContainer.append(GeneralUtility.createAlert('danger', 'File could not be imported: ' + e, 5000));
                }
            }, false);
            reader.readAsText(file);
        });
    }
}
