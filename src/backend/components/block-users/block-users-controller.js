/**
 * Controls block user options.
 *
 * @author FiveFuenf
 */
class BlockUsersController extends AbstractController {
    constructor(view) {
        super(view);

        this.view.on('save', (e) => this.save(e));
        this.view.on('remove', (e) => this.remove(e));
    }


    /**
     * Saves a new blocked user.
     *
     * @param {*} event Event parameters
     *
     * @returns {void}
     * @throws When the name already exists
     */
    save(event) {
        let blockUser = new BlockUser(event.name);
        let blockUsers = Settings.getInstance().get('blockUsers');

        // If this is a call for a fresh, not yet saved blocked user, check if  name already exists
        if (event.isNew !== undefined && event.isNew && blockUsers.hasOwnProperty(blockUser.name)) {
            throw new Error('User with name "' + blockUser.name + '" is already blocked.');
        }

        // If the blocked user was already saved, but the name is changed, remove  old name
        if (event.name !== event.previousName) {
            this.remove(event.previousName);
        }

        blockUsers[blockUser.name] = blockUser.serialize();
        Settings.getInstance().set('blockUsers', blockUsers);
    }


    /**
     * Removes a blocked user.
     *
     * @param {string} name Name of the user to unblock
     *
     * @returns {void}
     */
    remove(name) {
        let path = ['blockUsers', name];
        Settings.getInstance().remove(path);
    }
}
