/**
 * View for blocking users.
 *
 * @author FiveFuenf
 */
class SavePostsView extends AbstractView {
    constructor(container) {
        super(container);

        // Define possible events for observers
        this.observers = {
            'remove': []
        };

        // List all saved posts
        this.savePostList = container.querySelector('#savePostList');

        // No saved posts message
        this.noEntries = container.querySelector('.no-entries');

        // Markup is created directly in index.html // this.render()
        this.initializeDom();
    }


    initializeDom() {
        super.initializeDom();

        let savePosts = Settings.getInstance().get('savePosts');
        let savePostsKeys = Object.keys(savePosts);
        if (savePostsKeys.length === 0) {
            this.noEntries.classList.remove('d-none');
            return;
        }

        savePostsKeys.forEach(postId => {
            let savePostRow = this.creatSavePostRow(savePosts[postId]);
            this.savePostList.prepend(savePostRow);
        });
    }


    /**
     * Creates a new row showing a saved post.
     *
     * @param {SavePost} savePost Save post object
     *
     * @returns {Element}
     */
    creatSavePostRow(savePost) {
        let inputGroup = document.createElement('div');
        inputGroup.classList.add('input-group');

        // Controls
        let removeButton = document.createElement('button');
        removeButton.classList.add('btn', 'btn-danger');
        removeButton.setAttribute('type', 'button');
        removeButton.innerHTML = 'Remove';

        // Link
        let url = GeneralUtility.buildUrlToPost(savePost.postId, savePost.threadId);
        let linkButton = document.createElement('a');
        linkButton.classList.add('link', 'btn', 'btn-primary');
        linkButton.setAttribute('target', '_blank');
        linkButton.setAttribute('href', url);
        linkButton.innerHTML = 'Open';

        // Creation date
        let dateField = document.createElement('span');
        dateField.classList.add('date', 'form-control');
        dateField.innerHTML = DateUtility.formatTimestamp(savePost.timestamp);

        inputGroup.append(linkButton, dateField, removeButton);

        // Description below
        if (savePost.description) {
            let description = document.createElement('small');
            description.classList.add('description', 'w-100', 'd-block', 'pb-2');
            description.innerHTML = savePost.description;

            inputGroup.append(description);
        }

        // Setup events
        removeButton.addEventListener('click', () => {
            this.trigger('remove', savePost.postId);
            inputGroup.remove();
        });

        return inputGroup;
    }
}
